# inline-img
Replace img tags with the resource located at their src url.
Currently only tested with svg.

# installation
``` bash
  $ npm install -g inline-img
```

# usage
``` bash
  $ cat index.html | inline-img
```

## License
[MIT][license-url]

[license-url]: LICENSE

