#!/usr/bin/env node
;(function () {
  'use strict'

  /* imports */
  var request = require('request')
  var readline = require('readline')

  /* exports */

  inlineImg()
  
  function inlineImg () {
    var original = []
    var replacements = []

    var reader = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
      terminal: false
    })

    var counter = 0

    reader.on('line', function (line) {
      var n = original.push(line)

      var url = parseImgUrl(line)

      if (url) {
        counter++

        getImage(url, function (error, image) {
          var originalString = original[n - 1]

          var replacer = new RegExp('<img.*' + url + '.*?>')
          var replaced = originalString.replace(replacer, image)
          original[n - 1] = replaced
          counter--
          if (counter === 0) {
            print(original)
          }
        })
      }
    })
  }

  function parseImgUrl (line) {
    var match = line.match(/.*<img.*src="(.*)".*/)
    if (match) {
      return match[1]
    }
  }

  function getImage (url, callback) {
    var requestOptions = {
      url: url
    }
    request(requestOptions, function (error, response, body) {
      callback(error, body)
    })
  }

  function print (array) {
    array.forEach(function (line) {
      console.log(line)
    })
  }
})()

